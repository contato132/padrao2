import redirectRootToPortal from './route-redirect-portal'
const NomeProjetoGitLab = '/padrao2'//'/teste-padrao'

export default nuxtConfig => {
  const router = Reflect.has(nuxtConfig, 'router') ? nuxtConfig.router : {}
 // const base = Reflect.has(router, 'base') ? router.base : '/teste-padrao'
 const base = Reflect.has(router, 'base') ? router.base : NomeProjetoGitLab

  return {
    /**
     * 'render:setupMiddleware'
     * {@link node_modules/nuxt/lib/core/renderer.js}
     */
    setupMiddleware(app) {
      app.use('/', redirectRootToPortal(base))
    }
  }
}