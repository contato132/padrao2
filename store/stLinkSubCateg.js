
import { OrdenaPosicaoDeUmArray } from "../assets/uteis"

export const state = () => ({
        ListaLinkSubCategorias:[],
        
        
  })

  export const mutations = {
      SET_LinkSubCategorias(state,subcategorias){
        state.ListaLinkSubCategorias = subcategorias
        state.ListaLinkSubCategorias.sort(OrdenaPosicaoDeUmArray);
      },
      SET_OrdenaLinkSubCategorias(state){
        state.ListaLinkSubCategorias.sort(OrdenaPosicaoDeUmArray);
      },
      SET_PosicaoLinkSubCategorias(state,params){
        var indx = state.ListaLinkSubCategorias.findIndex(item => item.idlink === params.idlink);
        if (indx > -1) {
          state.ListaLinkSubCategorias[indx].posicao = params.posicao
         // console.log("ATUALIZOU SET SET_PosicaoCategorias = ",params.idcategoria);
        }
      },
      SET_StatusLinkSubCategorias(state,params){
        var indx = state.ListaLinkSubCategorias.findIndex(item => item.idlink === params.idlink);
        if (indx > -1) {
          state.ListaLinkSubCategorias[indx].status = params.status
         // console.log("ATUALIZOU SET SET_PosicaoCategorias = ",params.idcategoria);
        }
      },
      POST_LinkSubCategorias(state,params){
        state.ListaLinkSubCategorias.push(params)
      },
      DELETE_LinkSubCategorias(state,params){
        var indx = state.ListaLinkSubCategorias.findIndex(item => item.idlink === params.idlink);
        if (indx > -1) {
          state.ListaLinkSubCategorias.splice(indx, 1);
         // console.log("DELETOU titulo categ = ",params.idcategoria);
        }
      },
      
      
  }

export const actions = {

  async PopulaLinkSubCategorias({commit},params) {
            let RespPadrao = {}
            //let caminho = this.$axios.defaults.baseURL + "/padrao"   
            let caminho = this.$axios.defaults.baseURL + "/padrao"   
            
            let instrucao = "SELECT linksubcateg.idlink,linksubcateg.idcategoria,linksubcateg.idsubcategoria,"+
                                    " linksubcateg.idtipousuario,linksubcateg.idtipo, " +
                                    " linksubcateg.posicao,linksubcateg.status, " +
                                   " subcategorias.subcategoria " +
                             "FROM linksubcateg JOIN subcategorias ON linksubcateg.idsubcategoria = subcategorias.idsubcategoria " + 
                            " WHERE (idtipousuario = " + params.idtipousuario + ") AND " +
                                             " (idtipo = " + params.idrevenda + ");"
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",0); // 0 =select 1,2,3 insert,update,delete
           
            let  resp = await this.$axios.put(caminho,formData)
            if (resp.status === 200) {
              console.log(" SET_LinkSubCategorias>>>>>>>>>>>>>>>>>>>>", resp.data.dados);
                commit('SET_LinkSubCategorias',resp.data.dados)
                RespPadrao.Erro = false;
                
                return RespPadrao
            }  else {
                RespPadrao.Erro = true;
                RespPadrao.msgErro = 'error popular link sub-categorias'
                console.log("error popular link sub-categorias", err);

                return RespPadrao
            }  
    
     
  },
  async AtualizaPosicaoLinkCategorias({commit},params) {
    
           

            
    

  },
  async AtualizaStatusLinkSubCategoria({commit},params) {
    let RespPadrao = {}
    let caminho = this.$axios.defaults.baseURL + "/padrao"   
    console.log("paramscategoria", params);
            let instrucao = "UPDATE linksubcateg SET status = '" + params.status + "' WHERE idlink =" + params.idlink + ";";    
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",2); // 0 =select 1,2,3 insert,update,delete
            let  resp = await this.$axios.post(caminho,formData)
            if (resp.status === 200) {
                var paramsCateg = {idlink:params.idlink,status:params.status}
                commit('SET_StatusLinkSubCategorias',paramsCateg)
                RespPadrao.Erro = false;
                RespPadrao.id = params.idlink;
                return RespPadrao
            }  else {
                RespPadrao.Erro = true;
                RespPadrao.msgErro = 'Erro ao atualizar status sub-categorias'
                console.log("Erro ao atualizar status sub-categorias", err);

                return RespPadrao
            }  
         
  },
  async InsereLinkSubCategoria({commit},params) {
    let RespPadrao = {}
    let caminho = this.$axios.defaults.baseURL + "/padrao"   
            
            let instrucao = "INSERT INTO linksubcateg (idsubcategoria,idcategoria,idtipo,idtipousuario,posicao,status) "+
                     " VALUES(" + params.idsubcategoria + " , " + params.idcategoria +  " , " + params.idtipo  + " , " + params.idtipousuario  +  
                     " , " + params.posicao + " , " + params.status + ");";
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",1); // 0 =select 1,2,3 insert,update,delete
            let resp = await this.$axios.post(caminho,formData);
            if (resp.status === 200) {
              //console.log("resp inserção : ", resp.data.dados);
              var paramsCateg = {	idlink:resp.data.dados.insertId,
                                  idsubcategoria:params.idsubcategoria,
                                  idcategoria:params.idcategoria,
                                  subcategoria:params.subcategoria ,
                                  idtipo:params.idtipo,
                                  idtipousuario:params.idtipousuario,
                                  posicao:params.posicao,
                                  status:params.status}
              commit('POST_LinkSubCategorias',paramsCateg)
              RespPadrao.Erro = false;
              RespPadrao.id = resp.data.dados.insertId;
              
              return RespPadrao
            } else {
              RespPadrao.Erro = true;
              RespPadrao.msgErro = 'Erro ao inserir link sub-categorias'
              console.log("Erro ao inserir link sub-categorias", err);

              return RespPadrao
           };
  
  },
  async ExcluiSubCategoria({commit},params) {
    let RespPadrao = {}
    let caminho = this.$axios.defaults.baseURL + "/padrao"   
                
            let instrucao = "DELETE FROM linksubcateg WHERE idlink=" + params.idlink + ";";
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",3); // 0 =select 1,2,3 insert,update,delete
            let resp = await this.$axios.post(caminho,formData);
            if (resp.status === 200) {
                 // console.log("resp exclusão : ", resp);
                  var paramsCateg = {idlink:params.idlink}
                  commit('DELETE_LinkSubCategorias',paramsCateg)
                  RespPadrao.Erro = false;
                  RespPadrao.id = params.idlink;
                  return RespPadrao
                } else {
                  RespPadrao.Erro = true;
                  RespPadrao.msgErro = 'Erro ao excluir link sub-categorias'
                  console.log("Erro ao excluir link sub-categorias", err);

                  return erro
              };
  
  },
  
    
}


 