
import { OrdenaPosicaoDeUmArray } from "../assets/uteis"

export const state = () => ({
        ListaLinkSub2Categorias:[],
        
        
  })

  export const mutations = {
      SET_LinkSub2Categorias(state,sub2categorias){
        state.ListaLinkSub2Categorias = sub2categorias
        state.ListaLinkSub2Categorias.sort(OrdenaPosicaoDeUmArray);
      },
      SET_OrdenaLinkSubCategorias(state){
        state.ListaLinkSub2Categorias.sort(OrdenaPosicaoDeUmArray);
      },
      SET_PosicaoLinkSubCategorias(state,params){
        var indx = state.ListaLinkSub2Categorias.findIndex(item => item.idlink === params.idlink);
        if (indx > -1) {
          state.ListaLinkSub2Categorias[indx].posicao = params.posicao
         
        }
      },
      SET_StatusLinkSubCategorias(state,params){
        var indx = state.ListaLinkSub2Categorias.findIndex(item => item.idlink === params.idlink);
        if (indx > -1) {
          state.ListaLinkSub2Categorias[indx].status = params.status
         
        }
      },
      POST_LinkSubCategorias(state,params){
        state.ListaLinkSub2Categorias.push(params)
      },
      DELETE_LinkSubCategorias(state,params){
        var indx = state.ListaLinkSub2Categorias.findIndex(item => item.idlink === params.idlink);
        if (indx > -1) {
          state.ListaLinkSub2Categorias.splice(indx, 1);
         
        }
      },
      
      
  }

export const actions = {

  async PopulaLinkSub2Categorias({commit},params) {
            let RespPadrao = {}
            //let caminho = this.$axios.defaults.baseURL + "/padrao"   
            let caminho = this.$axios.defaults.baseURL + "/padrao"   
            
            let instrucao = "SELECT linksub2categ.idlink,linksub2categ.idsubcategoria,linksub2categ.idsub2categoria," +
                                    " linksub2categ.posicao,linksub2categ.status, " +
                                    " linksub2categ.idtipousuario,linksub2categ.idtipo, " +
                                   " sub2categorias.subcategoria " +
                             "FROM linksub2categ JOIN sub2categorias ON linksub2categ.idsub2categoria = sub2categorias.idsub2categoria " + 
                            "  WHERE (idtipousuario = " + params.idtipousuario + ") AND " +
                                             " (idtipo = " + params.idrevenda + ");"
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",0); // 0 =select 1,2,3 insert,update,delete
           
            let  resp = await this.$axios.put(caminho,formData)
            if (resp.status === 200) {
                commit('SET_LinkSub2Categorias',resp.data.dados)
                RespPadrao.Erro = false;
                
                return RespPadrao
            }  else {
                RespPadrao.Erro = true;
                RespPadrao.msgErro = 'error popular link sub-categorias'
                console.log("error popular link sub-categorias", err);

                return RespPadrao
            }  
    
     
  },
  async AtualizaPosicaoLinkCategorias({commit},params) {
    
           

            
    

  },
  async AtualizaStatusLinkSubCategoria({commit},params) {
    let RespPadrao = {}
    let caminho = this.$axios.defaults.baseURL + "/padrao"   
    console.log("paramscategoria", params);
            let instrucao = "UPDATE linksub2categ SET status = '" + params.status + "' WHERE idlink =" + params.idlink + ";";    
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",2); // 0 =select 1,2,3 insert,update,delete
            let  resp = await this.$axios.post(caminho,formData)
            if (resp.status === 200) {
                var paramsCateg = {idlink:params.idlink,status:params.status}
                commit('SET_StatusLinkSubCategorias',paramsCateg)
                RespPadrao.Erro = false;
                RespPadrao.id = params.idlink;
                return RespPadrao
            }  else {
                RespPadrao.Erro = true;
                RespPadrao.msgErro = 'Erro ao atualizar status sub-categorias'
                console.log("Erro ao atualizar status sub-categorias", err);

                return RespPadrao
            }  
         
  },
  async InsereLinkSub2Categoria({commit},params) {
    let RespPadrao = {}
    let caminho = this.$axios.defaults.baseURL + "/padrao"   
            
            let instrucao = "INSERT INTO linksub2categ (idsubcategoria,idsub2categoria,idtipo,idtipousuario,posicao,status) "+
                     " VALUES(" + params.idsubcategoria + " , " + params.idsub2categoria +  " , " + params.idtipo  + " , " + params.idtipousuario  +  
                     " , " + params.posicao + " , " + params.status + ");";
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",1); // 0 =select 1,2,3 insert,update,delete
            let resp = await this.$axios.post(caminho,formData);
            if (resp.status === 200) {
              //console.log("resp inserção : ", resp.data.dados);
              var paramsCateg = {	idlink:resp.data.dados.insertId,
                                  idsubcategoria:params.idsubcategoria,
                                  idsub2categoria:params.idsub2categoria,
                                  subcategoria:params.sub2categoria ,
                                  idtipo:params.idtipo,
                                  idtipousuario:params.idtipousuario,
                                  posicao:params.posicao,
                                  status:params.status}
              commit('POST_LinkSubCategorias',paramsCateg)
              RespPadrao.Erro = false;
              RespPadrao.id = resp.data.dados.insertId;
              
              return RespPadrao
            } else {
              RespPadrao.Erro = true;
              RespPadrao.msgErro = 'Erro ao inserir link sub-categorias'
              console.log("Erro ao inserir link sub-categorias", err);

              return RespPadrao
           };
  
  },
  async ExcluiSub2Categoria({commit},params) {
    let RespPadrao = {}
    let caminho = this.$axios.defaults.baseURL + "/padrao"   
                
            let instrucao = "DELETE FROM linksub2categ WHERE idlink=" + params.idlink + ";";
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",3); // 0 =select 1,2,3 insert,update,delete
            let resp = await this.$axios.post(caminho,formData);
            if (resp.status === 200) {
                 // console.log("resp exclusão : ", resp);
                  var paramsCateg = {idlink:params.idlink}
                  commit('DELETE_LinkSubCategorias',paramsCateg)
                  RespPadrao.Erro = false;
                  RespPadrao.id = params.idlink;
                  return RespPadrao
                } else {
                  RespPadrao.Erro = true;
                  RespPadrao.msgErro = 'Erro ao excluir link sub-categorias'
                  console.log("Erro ao excluir link sub-categorias", err);

                  return erro
              };
  
  },
  
    
}


 