import { OrdenaPosicaoDeUmArray } from "../assets/uteis"

export const state = () => ({
    
    EdicaoSite:false, // não utilizo mas como no editor possui por segurança fica sempre false para evitar alterações nos componentes
    LarguraTela:0,
    SideBarAtiva:false,
    InputPesquisaAberta:false,
    PercentualCompletoArquivo:0,
    AreaEdicaoConfig:'',
    TituloAreaEdicao:'',
    KeyImgsLoadFetch:'KeyImgsLoadFetch',
   // ListaCategorias:[],
    ListaSubCategorias:[],
    ListaSub2Categorias:[],
    idcatgsite :4, 
    idsubcategsite:7,
    idsub2categsite:0,
    
    
    
})

export const mutations = {
    
    SET_SideBarAtiva(state,SideBarAtivada){
        state.SideBarAtiva = SideBarAtivada
    },
    SET_InputPesquisaAberta(state,InputPesquisaAberta){
        state.InputPesquisaAberta = InputPesquisaAberta
    },

    SET_PercentualCompletoArquivo(state,Percentual){
        state.PercentualCompletoArquivo = Percentual
        //state.ListaProdutosVenda.sort(OrdenaPosicaoDeUmArray);
    },
    SET_AreaEdicaoConfig(state,AreaEdicao){
        state.AreaEdicaoConfig = AreaEdicao
        //state.ListaProdutosVenda.sort(OrdenaPosicaoDeUmArray);
    },
    SET_TituloAreaEdicao(state,TitAreaEdicao){
        state.TituloAreaEdicao = TitAreaEdicao
        //state.ListaProdutosVenda.sort(OrdenaPosicaoDeUmArray);
    },
    SET_KeyImgsLoadFetch(state,KeyImgsLoadFetch){
        state.KeyImgsLoadFetch = KeyImgsLoadFetch
        //state.ListaProdutosVenda.sort(OrdenaPosicaoDeUmArray);
    },
 /*   SET_Categorias(state,categorias){
        state.ListaCategorias = categorias
        state.ListaCategorias.sort(OrdenaPosicaoDeUmArray);
    },*/
    SET_SubMenuAberto(state,params){
        var indx = state.ListaCategorias.findIndex(item => item.idcategoria === params.idcategoria);
        if (indx > -1) {
          if (state.ListaCategorias[indx].submenuaberto === true)
              state.ListaCategorias[indx].submenuaberto = false
          else
             state.ListaCategorias[indx].submenuaberto = true
          //console.log("ATUALIZOU SET Menuabertooufchado = ",params);
        }
    },
    SET_Sub2MenuAberto(state,params){
        var indx = state.ListaSubCategorias.findIndex(item => item.idsubcategoria === params.idsubcategoria);
        if (indx > -1) {
          if (state.ListaSubCategorias[indx].submenuaberto === true)
              state.ListaSubCategorias[indx].submenuaberto = false
          else
             state.ListaSubCategorias[indx].submenuaberto = true
          //console.log("ATUALIZOU SET Menuabertooufchado = ",params);
        }
    },
    SET_SubCategorias(state,subcategorias){ 
        state.ListaSubCategorias = subcategorias
        state.ListaSubCategorias.sort(OrdenaPosicaoDeUmArray);
    },
    SET_Sub2Categorias(state,sub2categorias){ 
        state.ListaSub2Categorias = sub2categorias
        state.ListaSub2Categorias.sort(OrdenaPosicaoDeUmArray);
    },
    SET_LarguraTela(state,Largura){
      state.LarguraTela = Largura
    },
  
    
    
}


export const actions = {
    async UPLOADFile({commit},params) {
        let RespPadrao = {}
        let caminho = this.$axios.defaults.baseURL + "/padraofile"   
                
                console.log("params.arquivo upload : ", params.arquivo);
                let formData = new FormData()             
                formData.append("arquivo",params.arquivo);
                formData.append("arquivoanterior",params.arquivoanterior);
                formData.append("excluirarqanterior",params.ExcluirArqAnterior);
                
               /* let resp = await this.$axios.post(caminho,formData,
                   { headers: {'Content-Type': 'multipart/form-data'}});*/
    
                 const config = {
                    onUploadProgress: (progressEvent) => {
                        var PercentualCompletoArquivo = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                        commit('SET_PercentualCompletoArquivo',PercentualCompletoArquivo)    
                        if (PercentualCompletoArquivo === 100)
                            commit('SET_PercentualCompletoArquivo',0)    
                        console.log("percentCompleted : ",PercentualCompletoArquivo);
                        //do something with the percentCompleted
                        //I used an observable to pass the data to a component and subscribed to it, to fill the progressbar
                    }
                }    
    
                let resp = await this.$axios.post(caminho,formData,config);   
                if (resp.status === 200) {
                  console.log("resp upload : ", resp);
                  
                 // commit('POST_ProdutosVenda',paramsProd)
                  RespPadrao.Erro = false;
                  RespPadrao.NomeArqServidor = resp.data.arquivoServidor
                  
                  return RespPadrao
                } else {
                  RespPadrao.Erro = true;
                  RespPadrao.msgErro = 'Erro ao fazer upload do arquivo!'
                  //console.log("Erro ao inserir produto", err);
    
                  return RespPadrao
               };
      
      },
    async CommitLarguraTela({commit},Largura) {
        commit('SET_LarguraTela',Largura)
    }, 
   
    async CommitAreaEdicaoConfig({commit},AreaEdicao) {
        commit('SET_AreaEdicaoConfig',AreaEdicao)
    },
    async CommitTituloAreaEdicao({commit},TitAreaEdicao) {
        commit('SET_TituloAreaEdicao',TitAreaEdicao)
    },
    async CommitSideBarAtiva({commit},SideBarAtivada) {
        commit('SET_SideBarAtiva',SideBarAtivada)
    }, 
    async CommitInputPesquisaAberta({commit},InputPesquisaAberta) {
        commit('SET_InputPesquisaAberta',InputPesquisaAberta)
    }, 
    async CommitKeyImgsLoadFetch({commit},KeyImgsLoadFetch) {
        commit('SET_KeyImgsLoadFetch',KeyImgsLoadFetch)
    }, 
   /* async CommitCategoria({commit},params) {
      
        commit('SET_Categorias',params.dados)
    },*/
    async CommitSubMenuAberto({commit},params) {
        
      commit('SET_SubMenuAberto',params)
    },
    async CommitSub2MenuAberto({commit},params) {
        
        commit('SET_Sub2MenuAberto',params)
      },
    async CommitSubCategoria({commit},params) {
      
        commit('SET_SubCategorias',params.dados)
    },
    async CommitSub2Categoria({commit},params) {
      
        commit('SET_Sub2Categorias',params.dados)
    },
    
}