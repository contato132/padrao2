

export const state = () => ({
    
    SiteConfiguracoes:[],
    ObjConfig:{}    
})

export const mutations = {
 
 SET_SiteConfiguracoes(state,SiteConfig){
     state.SiteConfiguracoes = SiteConfig
 },
 SET_CampoConfigSite(state,configStr){ 
   //  console.log("ANTES  SET_ConfigSite = ",configStr);
     if (state.SiteConfiguracoes.length === 1) {
             state.SiteConfiguracoes[0].configsite = configStr
        
      // console.log("ATUALIZOU SET_LogoConfigSite = ",params.logoarqservidor);
     }
     
 },
 SET_ObjConfig(state,objConfig){
     if (state.SiteConfiguracoes.length === 1) {
         state.ObjConfig = objConfig   
     }
 },
 
}


export const actions = {
 
 async PesquisaSiteConfig({commit},params) {
     let RespPadrao = {}
     
     let caminho = this.$axios.defaults.baseURL + "/padrao"   
    
     let instrucao = "SELECT configsite	 "+ 
                    "FROM revendas " + 
                    "WHERE idrevenda = " + params.idrevenda + ";"
     let formData = new FormData()             
     formData.append("instrucaoSQL",instrucao);
     formData.append("tipoInstrucao",0); // 0 =select 1,2,3 insert,update,delete
    
     let  resp = await this.$axios.put(caminho,formData)
     
     if (resp.status === 200) {
       //console.log("resp idsiteconfig", resp);
        commit('SET_SiteConfiguracoes',resp.data.dados)
        if (resp.data.dados.length === 1)
             commit('SET_ObjConfig',JSON.parse(resp.data.dados[0].configsite))
        

        RespPadrao.dados = resp.data.dados;
        RespPadrao.Erro = false;
        return RespPadrao
     }  else {
         RespPadrao.Erro = true;
         RespPadrao.msgErro = 'error pesquisar configurações site'
         console.log("error pesquisar configurações site", err);

         return RespPadrao
     }  

 }, 
 async AtualizaCampoConfigSite({commit},params) {
     let RespPadrao = {}
     let caminho = this.$axios.defaults.baseURL + "/padrao"   
    
             let instrucao = "UPDATE revendas SET configsite = '" + JSON.stringify(params.configsite) + "'" +
                              " WHERE idrevenda =" + params.idrevenda + ";";    
             let formData = new FormData()             
             formData.append("instrucaoSQL",instrucao);
             formData.append("tipoInstrucao",2); // 0 =select 1,2,3 insert,update,delete
             let  resp = await this.$axios.post(caminho,formData)
             console.log("Antes em atualiza configsite", resp);
             if (resp.status === 200) {
                
                 commit('SET_CampoConfigSite',JSON.stringify(params.configsite)) 
                 commit('SET_ObjConfig',params.configsite) 
                 RespPadrao.Erro = false;
                 return RespPadrao
             }  else {
                 RespPadrao.Erro = true;
                 RespPadrao.msgErro = 'Erro ao atualizar configurações do site'
                 console.log("Erro ao atualizar configurações do site", err);
 
                 return RespPadrao
             }  
          
 },
 
  
 
}