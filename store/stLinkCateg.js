
import { OrdenaPosicaoDeUmArray } from "../assets/uteis"

export const state = () => ({ 
        ListaLinkCategorias:[],
        
        
  })

  export const mutations = {
      SET_LinkCategorias(state,categorias){
        state.ListaLinkCategorias = categorias
        state.ListaLinkCategorias.sort(OrdenaPosicaoDeUmArray);
      },
      SET_OrdenaLinkCategorias(state){
        state.ListaLinkCategorias.sort(OrdenaPosicaoDeUmArray);
      },
      SET_PosicaoLinkCategorias(state,params){
        var indx = state.ListaLinkCategorias.findIndex(item => item.idlink === params.idlink);
        if (indx > -1) {
          state.ListaLinkCategorias[indx].posicao = params.posicao
         // console.log("ATUALIZOU SET SET_PosicaoCategorias = ",params.idcategoria);
        }
      },
      SET_StatusLinkCategorias(state,params){
        var indx = state.ListaLinkCategorias.findIndex(item => item.idlink === params.idlink);
        if (indx > -1) {
          state.ListaLinkCategorias[indx].status = params.status
         // console.log("ATUALIZOU SET SET_PosicaoCategorias = ",params.idcategoria);
        }
      },
      POST_LinkCategorias(state,params){
        state.ListaLinkCategorias.push(params)
        
      },
      DELETE_LinkCategorias(state,params){
        var indx = state.ListaLinkCategorias.findIndex(item => item.idlink === params.idlink);
        if (indx > -1) {
          state.ListaLinkCategorias.splice(indx, 1);
         // console.log("DELETOU titulo categ = ",params.idcategoria);
        }
      },
      
      
      
  }

export const actions = {

  async PopulaLinkCategorias({commit},params) {
            let RespPadrao = {}
            //let caminho = this.$axios.defaults.baseURL + "/padrao"   
            let caminho = this.$axios.defaults.baseURL + "/padrao"   
            
            let instrucao = "SELECT linkcateg.idlink,linkcateg.idcategoria,linkcateg.posicao,linkcateg.status, " +
                            " linkcateg.idtipousuario,linkcateg.idtipo, " +
                                   " categorias.categoria " +
                             " FROM linkcateg JOIN categorias ON linkcateg.idcategoria = categorias.idcategoria " + 
                            " WHERE (linkcateg.idtipousuario = " + params.idtipousuario + ") AND " +
                                             " (linkcateg.idtipo = " + params.idrevenda + ");"
           
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",0); // 0 =select 1,2,3 insert,update,delete
           
            let  resp = await this.$axios.put(caminho,formData)
            if (resp.status === 200) {
           
                commit('SET_LinkCategorias',resp.data.dados)
                RespPadrao.Erro = false;
                
                return RespPadrao
            }  else {
                RespPadrao.Erro = true;
                RespPadrao.msgErro = 'error popular link categorias'
                console.log("error popular link categorias", err);

                return RespPadrao
            }  
    
     
  },
  async AtualizaPosicaoLinkCategorias({commit},params) {
    
           

            
    

  },
  async AtualizaStatusLinkCategoria({commit},params) {
    let RespPadrao = {}
    let caminho = this.$axios.defaults.baseURL + "/padrao"   
    console.log("paramscategoria", params);
            let instrucao = "UPDATE linkcateg SET status = '" + params.status + "' WHERE idlink =" + params.idlink + ";";    
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",2); // 0 =select 1,2,3 insert,update,delete
            let  resp = await this.$axios.post(caminho,formData)
            if (resp.status === 200) {
                var paramsCateg = {idlink:params.idlink,status:params.status}
                commit('SET_StatusLinkCategorias',paramsCateg)
                RespPadrao.Erro = false;
                RespPadrao.id = params.idlink;
                return RespPadrao
            }  else {
                RespPadrao.Erro = true;
                RespPadrao.msgErro = 'Erro ao atualizar status categoria'
                console.log("Erro ao atualizar status categoria", err);

                return RespPadrao
            }  
         
  },
  async InsereLinkCategoria({commit},params) {
    let RespPadrao = {}
    let caminho = this.$axios.defaults.baseURL + "/padrao"   
            
            let instrucao = "INSERT INTO linkcateg (idcategoria,idtipo,idtipousuario,posicao,status) "+
                     " VALUES(" + params.idcategoria +  " , " + params.idtipo  + " , " + params.idtipousuario  +  
                     " , " + params.posicao + " , " + params.status + ");";
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",1); // 0 =select 1,2,3 insert,update,delete
            let resp = await this.$axios.post(caminho,formData);
            if (resp.status === 200) {
              //console.log("resp inserção : ", resp.data.dados);
              var paramsCateg = {	idlink:resp.data.dados.insertId,idcategoria:params.idcategoria,
                                  categoria:params.categoria,
                                  idtipo:params.idtipo,idtipousuario:params.idtipousuario,
                                  posicao:params.posicao,status:params.status}
              commit('POST_LinkCategorias',paramsCateg)
              RespPadrao.Erro = false;
              RespPadrao.id = resp.data.dados.insertId;
              RespPadrao.posicao = 2000;
              return RespPadrao
            } else {
              RespPadrao.Erro = true;
              RespPadrao.msgErro = 'Erro ao inserir link categoria'
              console.log("Erro ao inserir link categoria", err);

              return RespPadrao
           };
  
  },
  async ExcluiCategoria({commit},params) {
    let RespPadrao = {}
    let caminho = this.$axios.defaults.baseURL + "/padrao"   
                
            let instrucao = "DELETE FROM linkcateg WHERE idlink=" + params.idlink + ";";
            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",3); // 0 =select 1,2,3 insert,update,delete
            let resp = await this.$axios.post(caminho,formData);
            if (resp.status === 200) {
                 // console.log("resp exclusão : ", resp);
                  var paramsCateg = {idlink:params.idlink}
                  commit('DELETE_LinkCategorias',paramsCateg)
                  RespPadrao.Erro = false;
                  RespPadrao.id = params.idlink;
                  return RespPadrao
            } else {
                  RespPadrao.Erro = true;
                  RespPadrao.msgErro = 'Erro ao excluir link categoria'
                  console.log("Erro ao excluir link categoria", err);

                  return RespPadrao
            };
  }
  
    
}


 