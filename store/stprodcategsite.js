
import { OrdenaPosicaoDeUmArray } from "../assets/uteis"

export const state = () => ({
        ProdutosCategSite:null,
        
        
  })

  export const mutations = {
      SET_ProdutosCategSite(state,ProdCategSite){
        state.ProdutosCategSite = ProdCategSite
        state.ProdutosCategSite.sort(OrdenaPosicaoDeUmArray);
      },
      SET_OrdenaProdutosCategSite(state){
        state.ProdutosCategSite.sort(OrdenaPosicaoDeUmArray);
      },
      SET_PosicaoProdutosCategSite(state,params){
        var indx = state.ProdutosCategSite.findIndex(item => item.idprodcategsite === params.idprodcategsite);
        if (indx > -1) {
          state.ProdutosCategSite[indx].posicao = params.posicao
         // console.log("ATUALIZOU SET SET_PosicaoCategorias = ",params.idcategoria);
        }
       
        
        
      },
      SET_PrecoProdutoCategSite(state,params){
        var indx = state.ProdutosCategSite.findIndex(item => item.idprodcategsite === params.idprodcategsite);
        if (indx > -1) {
          state.ProdutosCategSite[indx].precovenda = params.precovenda
          console.log("ATUALIZOU SET preco = ",params.precovenda);
        }
      },
      POST_ProdCategSite(state,params){
        state.ProdutosCategSite.push(params)
      },
      DELETE_ProdutoCategSite(state,params){
        var indx = state.ProdutosCategSite.findIndex(item => item.idprodcategsite === params.idprodcategsite);
        if (indx > -1) {
          state.ProdutosCategSite.splice(indx, 1);
          console.log("DELETOU titulo categ = ",params.idprodcategsite);
        }
      },
      
      
  }

export const actions = {

  async PopulaProdutoCategSite({commit},params) {
            let RespPadrao = {}
            //let caminho = this.$axios.defaults.baseURL + "/padrao"   
            let caminho = this.$axios.defaults.baseURL + "/padrao"   
            
            

            let instrucao = "SELECT produto.idproduto,produto.produto, linkprodcateg.idlinkprodcateg, "+ 
                           " linkprodcateg.idcategoria,linkprodcateg.idsubcategoria,linkprodcateg.idsub2categoria,"+
                           " linkprodcateg.precovenda, linkprodcateg.posicao ,"+ 
                           " linkprodcateg.status "+
                           "FROM linkprodcateg JOIN produto ON linkprodcateg.idproduto = produto.idproduto " + 
                           "WHERE (linkprodcateg.idtipo = " + params.idrevenda  + ") AND (linkprodcateg.idtipousuario = 10);"


            let formData = new FormData()              
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",0); // 0 =select 1,2,3 insert,update,delete
           
            let  resp = await this.$axios.put(caminho,formData)
            if (resp.status === 200) {
                console.log("resp SET_ProdutosCategSite site", resp);
                commit('SET_ProdutosCategSite',resp.data.dados)
                RespPadrao.Erro = false;
                RespPadrao.dados = resp.data.dados
              
                return RespPadrao
            }  else {
                RespPadrao.Erro = true;
                RespPadrao.msgErro = 'error popular produtos site'
                console.log("error popular produtos site", err);

                return RespPadrao
            }  
    
     
  },
  async InsereProdutoCategSite({commit},params) {
    let RespPadrao = {}
    let caminho = this.$axios.defaults.baseURL + "/padrao"   
            
            let instrucao = "INSERT INTO linkprodcateg (idproduto,idsiteconfig,idcatgsite,idsubcategsite,"+
                             "idsub2categsite,precovenda,posicao,status) "+ 
                             " VALUES('" + params.idproduto +  "' , " + params.idsiteconfig + ", " + params.idcatgsite +  ", " +
                              params.idsubcategsite +  ", " + params.idsub2categsite +  ", " + params.precovenda +  ", " +
                              params.posicao +  ", " + params.status +");";

            let formData = new FormData()             
            formData.append("instrucaoSQL",instrucao);
            formData.append("tipoInstrucao",1); // 0 =select 1,2,3 insert,update,delete
            let resp = await this.$axios.post(caminho,formData);
            if (resp.status === 200) {
              console.log("resp inserção linkprodcateg : ", resp.data.dados);
              
              var paramsCateg = {idprodcategsite:resp.data.dados.insertId,
                                idproduto: parseInt(params.idproduto),produto: params.produto,
                                idcatgsite:params.idcatgsite, idsubcategsite:params.idsubcategsite,idsub2categsite:params.idsub2categsite,
                                precovenda:params.precovenda,  posicao:2000,status:params.status}
              commit('POST_ProdCategSite',paramsCateg)
              RespPadrao.Erro = false;
              RespPadrao.id = resp.data.dados.insertId;
              
              return RespPadrao
            } else {
              RespPadrao.Erro = true;
              RespPadrao.msgErro = 'Erro ao inserir produto para categoria'
              console.log("Erro ao inserir produto para categoria", err);

              return RespPadrao
           };
  
  },

}


 