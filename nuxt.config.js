import hooks from './hooks'
const NomeProjetoGitLab = '/padrao2'//'/teste-padrao'
const idAssociacao = 1
const idrevenda = 1

export default { 
  // Target: https://go.nuxtjs.dev/config-target
 target: 'static', 
 ssr: false,
 server: {
      port: 3005 //,21148//, // default: 3000
     // host: 'conexaoaprender.com.br'//process.env.HOST // host: '127.0.0.1' // default: localhost
  }, 
  /*env: {
    NOME_PROJ_GITLAB: '/teste-planeta',
    API_SECRETA:'123',
    ENV_PUBLICA:'envpub 123456'
  },*/
  dev: process.env.NODE_ENV !== 'production',
  publicRuntimeConfig: { // acessa através do this.$config em tempo de execução .env obsoleto
    
    NomeProjGitLab : NomeProjetoGitLab,
    idAssociacao:idAssociacao,
    idrevenda:idrevenda,
    baseStatics: process.env.NODE_ENV === 'production' ? NomeProjetoGitLab : '' ,
    
  },
  privateRuntimeConfig: {
    
  },
  router: {
    // solucionando o problema do gitlab na alteração da base / para /<nome do projeto>
    // https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-hooks#redirect-to-routerbase-when-not-on-root
   //  base: process.env.NODE_ENV === 'production' ? process.env.NOME_PROJ_GITLAB : '/' 
      base: process.env.NODE_ENV === 'production' ? (NomeProjetoGitLab +'/') : '/' 
   // base: '/teste-padrao/'
  },
 // hooks: hooks(this), // faz parte da solução para o gitlab que altera a base da url se digitar a raiz. Habilitar em produção
  generate: {
    //exclude: ['/contato']
    dir: 'public',
  },
  
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Revenda',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      
    ],
    script: [
     
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/global.css', 
    '~/assets/fonts/FredokaOne.css', 
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
   
    
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    
    //'@nuxtjs/dotenv' //obsoleto  https://levelup.gitconnected.com/what-are-env-files-and-how-to-use-them-in-nuxt-7f194f083e3d
  ],
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    //'@nuxtjs/pwa',
    
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
      baseURL: 'https://conexaoaprender.com.br', 
   // baseURL: 'http://localhost:3001', 
    
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa depois testar
 /* pwa: {
    manifest: {
      lang: 'pt-BR'
    }
  },*/

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    
  }
}
